# CS 348-01 Homework 4 Base

## Original code

- [Commit link](356f672e56bfe6fd801bdb0c537dc36547c61d3c)

### Meaningful Names

- Use intention revealing names & make meaningful distinctions
    - [Renamed string1 -> prefix](f2ee2c97081aff06f89545aa297600d66c38ff12) \
 PrefixTester.java lines: 9,10,14,16,17,19,20,26,28
    - [Renamed string2 -> word](c49aff7bebe44d89e1f5d23a778a4c81c99d7e37) \
 PrefixTester.java lines: 12-14,16-17,20,26,28

### Functions

- Use descriptive names
    - [Renamed binary() -> binaryToDecimal()](0c90e368585ec3ac81fb2c37b3eeb5f42c1e08e0) \
 Binomial2Decimal.java line 55
    - [Renamed decimal() -> decimalToBinary()](0c90e368585ec3ac81fb2c37b3eeb5f42c1e08e0) \
 Binomial2Decimal.java line 73
- Don't repeat yourself
    - [Implemented unused functions to remove repeated code](d2cf83207f6857357450bbbff85c19c3b6b81049) \
 Binomial2Decimal.java lines: 3-27,51-85

### Comments

- Redundant comments
    - [Removed needless comment](9f1100cc4e38de53b6e6dfbb656e555625fe3cb3) \
 Binomial2Decimal.java line 6
- Position marker
    - [Removed position marker](cd72f850e3a8d2315aafcc53c056985ff2d04c7c) \
 Binomial2Decimal.java line 21

### Formatting

- Vertical formatting
    - [Reformatted code vertically](f3a15127adf2b624b9fb87435d717abf1f4192bd) \
 PrefixTester.java lines: 2,8,13,17,26,28,36-37
- Horizontal formatting
    - [Reformatted code horizontally](f3a15127adf2b624b9fb87435d717abf1f4192bd) \
 PrefixTester.java lines: 18,20,22-24,26,30,32
