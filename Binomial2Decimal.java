import java.util.*;

public class Binomial2Decimal {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Binary to Decimal (1)");
        System.out.println("Decimal to Binary (2)");

        String choice = scan.nextLine();
        if (choice.charAt(0) == '1') {
            binaryToDecimal(scan);
        } else if (choice.charAt(0) == '2') {
            decimalToBinary(scan);
        }
    }

    public static void binaryToDecimal(Scanner scan) {
        System.out.println("Please enter Binary");
        String input = scan.nextLine();
        int length = input.length();
        int power = length - 1;
        int total = 0;

        for (int j = 0; j < length; j++) {
            char c_current = input.charAt(j);
            if (c_current == '1') {
                total = total + (int) (Math.pow(2, power - j));
            }
        }
        System.out.println(total);
    }

    public static void decimalToBinary(Scanner scan) {
        System.out.println("Please enter Decimal");
        int input = scan.nextInt();
        boolean zero = false;
        String total = "";

        for (int j = 262144; j >= 0; j = j / 2) {
            if (input >= j / 2) {
                total = total + '1';
                input = input - (j / 2);
                zero = true;
            }
            else {
                if (zero == true)
                    total = total + '0';
            }
            if (j < 2)
                break;
        }
        System.out.println(total.substring(0, total.length()-1));
    }

}