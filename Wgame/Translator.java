import java.awt.*; import javax.swing.*; import java.awt.event.*;

public class Translator extends JPanel implements ActionListener{
  
  JButton trans1 = new JButton("Common to Elysian");
  JButton trans2 = new JButton("Elysian to Common");
  JButton trans3 = new JButton("Common to Norric");
  JButton trans4 = new JButton("Norric to Common");
  JButton trans5 = new JButton("Norric to Elysian");
  JButton trans6 = new JButton("Elysian to Norric");
  JTextField input = new JTextField();
  JTextArea output = new JTextArea();
  JPanel p1 = new JPanel(); String swap;
  
  public Translator(){
    this.setLayout(new GridLayout(3,1));
    this.setPreferredSize(new Dimension(600,120));
    this.setBackground(Color.white);
    add(input); input.addActionListener(this);
    add(p1);
    p1.setLayout(new GridLayout(2,3));
    p1.add(trans1); trans1.addActionListener(this);
    p1.add(trans2); trans2.addActionListener(this);
    p1.add(trans4); trans4.addActionListener(this);
    p1.add(trans3); trans3.addActionListener(this);
    p1.add(trans6); trans6.addActionListener(this);
    p1.add(trans5); trans5.addActionListener(this);
    add(output);
  }
  
  public void actionPerformed(ActionEvent e){
    if(e.getSource() == trans1){
      c2e();
    }
    else if(e.getSource() == trans2){
      e2c();
    }
    else if(e.getSource() == trans3){
      c2n();
    }
    else if(e.getSource() == trans4){
      n2c();
    }
    else if(e.getSource() == trans5){
      swap = input.getText();
      n2c();
      input.setText(output.getText());
      c2e();
      input.setText(swap);
    }
    else if(e.getSource() == trans6){
      swap = input.getText();
      e2c();
      input.setText(output.getText());
      c2n();
      input.setText(swap);
    }
  }
  public void c2e(){
    boolean sp = false;
    char char1; char[] letters = {'i','d','f','m','a','h','p','g','o','w','s','k','n','t','y','r','x','l','c','b','e','z','j','q','u','v'};
    String str3 = "";
    String str2 = "";
    String str1 = input.getText()+' '; str1 = str1.toLowerCase();
    for(int j = 0; j < str1.length()-1; j++){
      if(str1.charAt(j) >= 'a'){
        if(str1.charAt(j+1) >= 'a'){
          str2 += str1.charAt(j);
        }
        else{
          if(str1.charAt(j) == 'e'){
            str3+= 'a'+str2;
          }
          else
            str3 += str1.charAt(j)+str2;
          str2 = "";
        }
      }
      else
        str3 += str1.charAt(j);
    }
    str1 = "";
    for(int j = 0; j < str3.length(); j++){
      if(str3.charAt(j) < 'a'){
        str1 += str3.charAt(j);
      }
      else
        str1 += letters[((int)(str3.charAt(j))-'a')];
    }
    output.setText(str1);
  }
  public void e2c(){
    char[] letters = {'e','t','s','b','u','c','h','f','a','w','l','r','d','m','i','g','x','p','k','n','y','z','j','q','o','v'};
    String str1 = ' '+input.getText()+' '; str1 = str1.toLowerCase(); String str2 = ""; String str3 = ""; char char1 = ' ';
    for(int j = 1; j < str1.length(); j++){
      if(str1.charAt(j) >= 'a'){
        if(str1.charAt(j-1) >= 'a'){
          str2 += str1.charAt(j);
        }
        else{
          if(str1.charAt(j) == 'i' && str1.charAt(j+1) < 'a'){
            char1 = 'i';
          }
          else if(str1.charAt(j) == 'i'){
            char1 = 'a';
          }
          else
            char1 = str1.charAt(j);
        }
      }
      else{
        if(str1.charAt(j-1) >= 'a'){
          str3 += str2+char1+str1.charAt(j);
          str2 = "";
        }
        else{
          str3 += str1.charAt(j);
        }
      }
    }
    str1 = "";
    for(int j = 0; j < str3.length(); j++){
      if(str3.charAt(j) < 'a'){
        str1 += str3.charAt(j);
      }
      else
        str1 += letters[((int)(str3.charAt(j))-'a')];
    }
    output.setText(str1);
  }
  public void c2n(){
    String str1 = input.getText(); str1 = str1.toLowerCase(); String str2 = "";
    char[] letters = {'e','d','c','b','i','h','g','f','o','n','m','l','k','j','u','t','s','r','q','p','y','x','v','w','a','z'};
    for(int j = 0; j < str1.length(); j++){
      if(str1.charAt(j) >= 'a'){
        str2 += letters[(int)str1.charAt(j)-'a'];
      }
      else{
        str2 += str1.charAt(j);
      }
    }
    output.setText(str2);
  }
  public void n2c(){
    String str1 = input.getText(); str1 = str1.toLowerCase(); String str2 = "";
    char[] letters = {'y','d','c','b','a','h','g','f','e','n','m','l','k','j','i','t','s','r','q','p','o','w','x','v','u','z'};
    for(int j = 0; j < str1.length(); j++){
      if(str1.charAt(j) >= 'a'){
        str2 += letters[(int)str1.charAt(j)-'a'];
      }
      else{
        str2 += str1.charAt(j);
      }
    }
    output.setText(str2);
  }
}