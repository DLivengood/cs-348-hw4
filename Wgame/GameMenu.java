import java.util.*; import javax.swing.*; import java.awt.*; import java.awt.event.*;
import java.io.*;

public class GameMenu extends JPanel implements ActionListener{
  
  JButton calc = new JButton("Calculator");
  JButton trans = new JButton("Translator");
  JButton map = new JButton("World Map");
  JButton quit = new JButton("Quit");
  
  public GameMenu(){
    setLayout(new GridLayout(4,1));
    setPreferredSize(new Dimension(200,300));
    setBackground(Color.white);
    add(calc); calc.addActionListener(this);
    add(trans); trans.addActionListener(this);
    add(map); map.addActionListener(this);
    add(quit); quit.addActionListener(this);
  }
  
  public void actionPerformed(ActionEvent e){
    if(e.getSource() == calc){
      JFrame frame1 = new JFrame("Attack Calculator");
      Container c1 = frame1.getContentPane();
      MyGameCalc window = new MyGameCalc();
      c1.add(window);
      frame1.pack();
      frame1.setVisible(true);
      frame1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }
    else if(e.getSource() == trans){
      JFrame frame2 = new JFrame ("Translator");
      Container c2 = frame2.getContentPane();
      Translator window2 = new Translator();
      c2.add(window2);
      frame2.pack();
      frame2.setVisible(true);
      frame2.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }
    else if(e.getSource() == map){
      JFrame frame3 = new JFrame("World Map");
      Container c3 = frame3.getContentPane();
      try{
      worldMap window3 = new worldMap();
      c3.add(window3);
      frame3.pack();
      frame3.setVisible(true);
      frame3.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
      }catch(IOException exp){
        System.exit(0);
      }
    }
    else if(e.getSource() == quit){
      System.exit(0);
    }
  }
}
