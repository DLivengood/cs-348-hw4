import java.awt.*; import javax.swing.*; import java.awt.event.*;

public class MyGameCalc extends JPanel implements ActionListener{
  
  JButton calc = new JButton("Calculate"); JTextArea ncalc = new JTextArea();
  JTextField min = new JTextField("0"); JTextArea nmin = new JTextArea("Min Attack");
  JTextField armor = new JTextField("0"); JTextArea narmor = new JTextArea("Armor");
  JTextField pierce = new JTextField("0.0"); JTextArea npierce = new JTextArea("Armor Piercing");
  JTextField roll = new JTextField("0"); JTextArea nroll = new JTextArea("Roll");
  Checkbox crit = new Checkbox("Critical Hit"); JTextArea ncrit = new JTextArea();
  JTextArea dam = new JTextArea(); JTextArea ndam = new JTextArea("Damage Done");
  JTextField percent = new JTextField("100"); JTextArea npercent = new JTextArea("%");
  
  public MyGameCalc(){
    this.setLayout(new GridLayout(2,8));
    this.setPreferredSize(new Dimension(800,50));
    this.setBackground(Color.white);
    add(nmin); add(narmor); add(npierce); add(nroll); add(ncrit); add(npercent); add(ncalc); add(ndam);
    this.add(min); min.addActionListener(this);
    this.add(armor); armor.addActionListener(this);
    this.add(pierce); pierce.addActionListener(this);
    this.add(roll); roll.addActionListener(this);
    this.add(crit);
    add(percent); percent.addActionListener(this);
    this.add(calc); calc.addActionListener(this);
    this.add(dam);
    
  }
  
  public void actionPerformed(ActionEvent e){
    if(e.getSource() == calc){
      int minn = Integer.parseInt(min.getText());
      int armorn = Integer.parseInt(armor.getText());
      double piercen = Double.parseDouble(pierce.getText());
      int rolln = Integer.parseInt(roll.getText());
      double percentn = Double.parseDouble(percent.getText())/100;
      int damm;
      if(crit.getState() == true){
        piercen = piercen*2;
        if(piercen > 100)
          piercen = 100;
        double pier = (100-piercen)/100;
        int att = (int)((minn+rolln)*percentn); double def1 = armorn*pier; double def2 = (100-Math.sqrt(def1))/100;
        damm = (int)(att*def2);
        damm = damm*2;
      }
      else{
        double pier = (100-piercen)/100;
        int att = (int)((minn+rolln)*percentn); double def1 = armorn*pier; double def2 = (100-Math.sqrt(def1))/100;
        damm = (int)(att*def2);
      }
      dam.setText(Integer.toString(damm));
    }
  }
}