//David Livengood 23023140

import java.util.*;

public class PrefixTester {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Enter two strings, test if first is prefix of second");
        System.out.println("Type a word");
        String prefix = scan.next();
        prefix = prefix.toLowerCase();

        System.out.println("and another");
        String word = scan.next();
        word = word.toLowerCase();

        System.out.println("Is " + prefix + " a prefix to " + word + "?");
        boolean isPrefix = true;
        if (prefix.length() > word.length()) {
            System.out.println("No, " + prefix + " is not a prefix to " + word);
        } else {
            for (int j = 0; j < prefix.length(); j++) {
                if (prefix.charAt(j) != word.charAt(j)) {
                    isPrefix = false;
                }
            }
            if (isPrefix) {
                System.out.println("Yes, " + prefix + " is a prefix to " + word);
            } else {
                System.out.println("No, " + prefix + " is not a prefix to " + word);
            }
        }
    }
}