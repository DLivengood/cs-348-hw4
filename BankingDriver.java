  //David Livengood 23023140
// runs java BankingDriver and creates BankingDriver.class
public class BankingDriver{
//begins the "main" class 
 public static void main(String[] args){
//creates nicks banking account with 400 checking and 500 savings
   Banking nicksAcct = new Banking("Nick",400,500);
//create carols banking account with 600 checking and 700 savings
   Banking carolsAcct = new Banking("Carol",600,700);
//prints out the total money from all the accounts
   System.out.println(nicksAcct.getTotalMoney()+carolsAcct.getTotalMoney());
//adds $100 to nicks checking
   nicksAcct.setCheckMoney(nicksAcct.getCheckMoney() + 100);
//prints nicks accounts stats (name,check,save)
   System.out.println(nicksAcct);
//adds all of nicks saving and checking to carols savings
   carolsAcct.setSaveMoney(carolsAcct.getSaveMoney() + nicksAcct.getCheckMoney() + nicksAcct.getSaveMoney());
//sets nicks savings to zero
   nicksAcct.setSaveMoney(nicksAcct.getSaveMoney() - nicksAcct.getSaveMoney());
//set nicks checking to zero
   nicksAcct.setCheckMoney(nicksAcct.getCheckMoney() - nicksAcct.getCheckMoney());
//prints out nicks stats
   System.out.println(nicksAcct);
//prints out carols stats
   System.out.println(carolsAcct);
  }
}